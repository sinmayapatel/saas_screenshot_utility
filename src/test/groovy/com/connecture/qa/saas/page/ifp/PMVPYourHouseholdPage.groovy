package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PMVPYourHouseholdPage extends ConnecturePage{
	static url = "Compare/GetStarted"
	
	static content = {
		firstNameTextBox(wait:true){$("input[id='MemberProfileDataModel_FirstName']")}
		lastNameTextBox(wait:true){$("input[id='MemberProfileDataModel_LastName']")}
		dateOfBirthTextBox(wait:true){$("input[id='MemberProfileDataModel_DateOfBirth']")}
		saveAndContinueBtn(wait:true){$("a[class='btn leftBlk nextBtn']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.MVPYourHouseHoldTitle
		   waitFor(){copyRightText.isDisplayed()}
	 }

	def "User verify YourHouseHold Details and click Save&Continue Btn"(loginData) {
		firstNameTextBox.firstElement().clear()
		firstNameTextBox << loginData.lastName
		lastNameTextBox.firstElement().clear()
		lastNameTextBox << loginData.lastName
		dateOfBirthTextBox.firstElement().clear()
		dateOfBirthTextBox << loginData.dob
		waitFor(){saveAndContinueBtn.isDisplayed()}
		saveAndContinueBtn.click()
	}
	
}
