package com.connecture.qa.saas.config

import geb.waiting.WaitTimeoutException
import org.openqa.selenium.WebDriverException

class ConnecturePage extends com.connecture.qa.config.ConnecturePage{
	
	static content = {
		smokerSpinner(required:false) {$("#_smoker")}
	}	
	
	@Override
	boolean verifyAt() {
		def result = super.verifyAt()
		waitFor {js.('document.readyState') == 'complete'}
		waitFor {js.('jQuery.active') == 0}
		return result
	}
	
	def "Handle HTTP Requests"(){
		waitFor {js.('document.readyState') == 'complete'}
		waitFor {js.('jQuery.active') == 0}
	}
	
	def "Handle the Mask"(element, seconds){
		try{
		 waitFor(seconds) { element.isDisplayed() }
		 waitFor { !element.isDisplayed() }
		} catch(WaitTimeoutException e) {
		 //Do Nothing
		}
	}
}

