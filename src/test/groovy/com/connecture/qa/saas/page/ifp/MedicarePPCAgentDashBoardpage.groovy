package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCAgentDashBoardpage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Profile/AgentDashboard"
	
	static content = {
		logoIcon(wait:true){$("img[title='Carrier Logo Icon']")[0]}
		startConsultationTab(wait:true){$("a[title='Start Consultation']")[0]}
	}
	
	static at = {
		logoIcon.isDisplayed()
		startConsultationTab.isDisplayed()
	}
	
	def "User selects startConsultation Tab"(){
		waitFor(){startConsultationTab.isDisplayed()}
		startConsultationTab.click()
	}
	
	def "User takes screenshot"(fileDir,pageRequired,currentSpecName){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(logoIcon.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(startConsultationTab.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,fileDir,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}

}
