package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PDentalEnrollmentFormPage extends ConnecturePage{
	static url = "Enroll/EnrollmentForm/Control117"
	
	static content = {
		agreeTermsAndConditionsCheckBox(wait:true){$("input[id='CheckBox_214_0']")}
		firstNameText1(wait:true){$("input[id='Input_131']")}
		lastNameText1(wait:true){$("input[id='Input_217']")}
		firstNameText2(wait:true){$("input[id='Input_222']")}
		lastNameText2(wait:true){$("input[id='Input_224']")}
		submitBtn(wait:true){$("input[id='Input_123']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.EnrollmentFormPageTitle
		   copyRightText.isDisplayed()
	 }
	
	def "User select I am the person and enter signature in Dental Enrollment Form"(employeeEnrollment) {
		agreeTermsAndConditionsCheckBox.click()
		firstNameText1 << employeeEnrollment.lastName
		lastNameText1 << employeeEnrollment.lastName
		firstNameText2 << employeeEnrollment.lastName
		lastNameText2 << employeeEnrollment.lastName
		submitBtn.click()
	}
}
