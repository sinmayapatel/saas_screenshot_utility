package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper
import java.io.File.*
import com.connecture.qa.saas.ifp.lsa.*

class IFPHomePage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	
	static content = {
		userName(wait:true){$('#PostData_UserName')}
		password(wait:true){$('#PostData_Password')}
		accessSite(wait:true){$('#lnkLogin')}
	}

	static at = {
		userName.isDisplayed()
		password.isDisplayed()
	}

	def "Enter Username and Password and Click On AccessSite link"(UserName,Password){
		userName << UserName
		password << Password
		accessSite.click()
	}
	
	def EnterUsernameAndPasswordAndClickOnAccessSitelink(UserName,Password){
		userName << UserName
		password << Password
		accessSite.click()
	}
	
	def takescrshot(currentSpecName,fileDir){
		if(userName.isDisplayed()) {
			if(driver.currentUrl.toString().contains("home")) {
				if(accessSite.text().contains("Access Site")) {
					def image = h.takeScreenshotSinglePage(this.browser.driver,fileDir,pageName,currentSpecName)
									}
			}
		}
		return image
	}
	
	

	def "User takes screenshot"(fileDir,pageRequired,currentSpecName){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(userName.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(accessSite.text().contains("Access Site")) {
						return h.takeScreenshotSinglePage(this.browser.driver,fileDir,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}
}
