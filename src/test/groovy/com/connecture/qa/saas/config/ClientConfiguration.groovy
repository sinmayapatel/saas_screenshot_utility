package com.connecture.qa.saas.config

saasClientSpecificInfo {
	IFP{
		
		IFPQA{
			LoginPageTitle = "InsureAdvantage - Welcome"
			HomePageTitle = "ACME Home"
			SpecialEnrollmentReasonsPageTitle = "Special Enrollment Reasons"
			CoverageQuestionPageTitle = "Coverage Question"
			DemographicsPageTitle = "ACME Get Started"
			SelectBrokerPageTitle = "ACME Broker Selection"
			ShoppingMethodPageTitle = "ACME Shopping Method"
			GuidedShoppingPageTitle = "ACME Guided Shopping"
			ViewPlansPageTitle = "ACME Available Plans"
			QuestionnairePage = "ACME Cigna Questionnaire"
			PlanDetailsPageTitle = "ACME Plan Details"
			ShoppingCartPageTitle = "ACME Shopping Cart"
			RegisterShopPageTitle = "ACME Create Account"
			EnrollmentFormPageTitle = "ACME Enrollment Form"
			EnrollmentConfirmationPageTitle = "ACME Enrollment Confirmation"
			STMEligibilityQuestionsPageTitle = "Short term medical plan eligibility questions"
			MVPHomePageTitle = "MVP -"
			QuickMemberRegistrationTitle = "Quick Member Registration"
			MVPYourHouseHoldTitle = "MVP - Your household"
			MedicalPageTitle = "MVP - Coverage Options"
			DentalPageTitle = "MVP - Coverage Options"
			CartPageTitle = "MVP - Coverage Options"
			EnrollmentFormPageTitle = "MVP - Enroll Form"
			
			
		}
		QA{
			LoginPageTitle = "ACME - Login"
			HomePageTitle = "ACME Home"
			SpecialEnrollmentReasonsPageTitle = "Special Enrollment Reasons"
			CoverageQuestionPageTitle = "Coverage Question"
			DemographicsPageTitle = "ACME Get Started"
			SelectBrokerPageTitle = "ACME Broker Selection"
			ShoppingMethodPageTitle = "ACME Shopping Method"
			GuidedShoppingPageTitle = "ACME Guided Shopping"
			ViewPlansPageTitle = "ACME Available Plans"
			QuestionnairePage = "ACME Cigna Questionnaire"
			PlanDetailsPageTitle = "ACME Plan Details"
			ShoppingCartPageTitle = "ACME Shopping Cart"
			RegisterShopPageTitle = "ACME Create Account"
			EnrollmentFormPageTitle = "ACME Enrollment Form"
			EnrollmentConfirmationPageTitle = "ACME Enrollment Confirmation"
			STMEligibilityQuestionsPageTitle = "Short term medical plan eligibility questions"
			MVPHomePageTitle = "MVP -"
			QuickMemberRegistrationTitle = "Quick Member Registration"
			MVPYourHouseHoldTitle = "MVP - Your household"
			MedicalPageTitle = "MVP - Coverage Options"
			DentalPageTitle = "MVP - Coverage Options"
			CartPageTitle = "MVP - Coverage Options"
			EnrollmentFormPageTitle = "MVP - Enroll Form"
			
			
		}
		
		STG{
			DemographicsPageTitle = "ACME Get Started"
			GuidedShoppingPageTitle = "ACME Guided Shopping"
			ViewPlansPageTitle = "ACME Available Plans"
			DetailsComparisonPageTitle = "ACME Plan Comparison"
			EnrollmentFormPageTitle = "ACME Enrollment Form"
			EnrollmentConfirmationPageTitle = "ACME Enrollment Confirmation"
		}
		
		Ansay{
			LoginPageTitle = "Lighthouse 46 - Login"
			HomePageTitle = "Lighthouse 46 Home"
			DemographicsPageTitle = "Lighthouse 46 Get Started"
			SelectBrokerPageTitle = "Lighthouse 46 Broker Selection" 
			ShoppingMethodPageTitle = "Lighthouse 46 Shopping Method"
			GuidedShoppingPageTitle = "Lighthouse 46 Guided Shopping"
			ViewPlansPageTitle = "Lighthouse 46 Available Plans"
			PlanDetailsPageTitle = "Lighthouse 46 Plan Details"
			DetailsComparisonPageTitle = "Lighthouse 46 Plan Comparison"
			ShoppingCartPageTitle = "Lighthouse 46 Shopping Cart"
			RegisterShopPageTitle = "Lighthouse 46 Create Account"
			EnrollmentFormPageTitle = "Lighthouse 46 Enrollment Form"
			EnrollmentConfirmationPageTitle = "Lighthouse 46 Enrollment Confirmation"
		}
	}
}