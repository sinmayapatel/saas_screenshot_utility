package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicareStartConsultationPage extends ConnecturePage{
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/StartConsultation"
	
	static content = {
		profileTab(wait:true){$("span[class='progress-inner']")[0]}
		sOATab(wait:true){$("a[title='Step 2 of 9 is for SOA']")[0]}
	}

	static at = {
		profileTab.isDisplayed()
		sOATab.isDisplayed()
	}
	
	def "User clicks on SOA tab"(){
		waitFor(){sOATab.isDisplayed()}
		sOATab.click()
	}
	
	def "User takes screenshot"(fileDir,pageRequired,currentSpecName){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(profileTab.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(sOATab.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,fileDir,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}
	
	
}
