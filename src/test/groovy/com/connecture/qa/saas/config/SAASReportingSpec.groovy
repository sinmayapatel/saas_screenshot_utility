package com.connecture.qa.saas.config

import geb.waiting.WaitTimeoutException
import com.connecture.qa.java.util.RandomValueUtils
import com.connecture.qa.java.util.connectmail.ConnectMail
import com.connecture.qa.java.util.exception.ConnectureCustomException
import groovy.ui.Console

@SuppressWarnings("deprecation")
class SAASReportingSpec extends com.connecture.qa.config.ConnectureReportingSpecification {

	def waitsForTabToSwitch(newTabTitle) {
		waitFor(20){ switchToWindow(newTabTitle) }
	}
	
	/*   Supporting GroovyConsole, this one method call will boot the console and freeze the test thread.
	 *   You can pass needed test objects as a map:  startGroovyConsole(['census':census])
	 *   In the GroovyConsole, resume the Geb spec thread via:  synchronized( browser ) { browser.notify() }
	 */
	def startGroovyConsole(supportObjects){
		// Pass supporting objects to console
		def console = new Console()
		console.setVariable('browser', browser)
		if ( supportObjects != null ){
			supportObjects.each {
				console.setVariable(it.key, it.value)
			}
		}
		// Load the console and a script template
		console.run()
		console.loadScriptFile(new File("src/test/resources/ConsoleGroovyTemplate.groovy"))
		
		// Freeze the Geb test thread
		synchronized( browser ) {
			browser.wait()
		}
		// In case of using in a 'then' block
		return true
	}
	
	def getRandomEmail(){
		def connect = new ConnectMail()
		def recipent = RandomValueUtils.getRandomAlphaNumeric(8)
		recipent = recipent + "@cnxqamail.dyndns.org"
		System.out.println("Your email address is: " + recipent)
		return recipent
	}
	
	def setNewConnectmail(email, password){
		def connectmail = new ConnectMail()
		connectmail.setEmailAccount(email, password)
		return connectmail
	}
	
	def loadProdAndStageConfigs() {
		// If working in QA, no need to load this config file
		if ( System.getProperty("geb.build.env").equals("QA")){
		 return
		}
		   
		// Inform users that the file must be present
		def extendedGebConfigFilePath = System.getProperty("extendedgebconfig.info.file")
		if ( new File(extendedGebConfigFilePath).exists() == false ){
		 throw new ConnectureCustomException("Users must request IFPLocalMachineATMConfigs.groovy and place it in the root folder")
		}
		
		// Load config file
		def extendedGebConfigFileUrl = new File(extendedGebConfigFilePath).toURI().toURL()
		return new ConfigSlurper().parse(extendedGebConfigFileUrl)
	  }
	
	def extendedGebConfig = loadProdAndStageConfigs()
}
