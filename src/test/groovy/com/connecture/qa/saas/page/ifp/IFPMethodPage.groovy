package com.connecture.qa.saas.page.ifp

import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class IFPMethodPage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Plan/Method"
	
	static at = {
		helpMeChoose.isDisplayed()
		howDoYouWantToShopForHealthInsurance.isDisplayed()	
	}

	static content = {
		helpMeChoose(wait:true){$("div[class='first col-md-7']>div>a[class='btn btn1']")}      //$("a[class='btn btn1']")[1]
		howDoYouWantToShopForHealthInsurance(wait:true){$("div[id='widget_vPP3v_PSZUiZBx1sYp2U0A']>h1")}	
	}
	
	def "User clicks on Help Me Choose button"(){
		waitFor(){helpMeChoose.isDisplayed()}
		helpMeChoose.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		
	if(pageRequired.equalsIgnoreCase("yes")){
		if(helpMeChoose.isDisplayed()) {
			if(driver.currentUrl.toString().contains(url)) {
				if(howDoYouWantToShopForHealthInsurance.text().contains("How do you want to shop for health insurance?")) {
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
				}
			}
		}else{
		
		}
	
	}
}}
