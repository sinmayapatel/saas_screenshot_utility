package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCSubsidyPage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/Subsidy"
	
	static content = {
		medicareExtraHelpText(wait:true){$('h1')[0]}
		drugsTab(wait:true){$("a[title='Step 5 of 9 is for Drugs']")[0]}
	}
	
	static at = {
		medicareExtraHelpText.isDisplayed()
	}

	def "User clicks on drugs tab"(){
		waitFor(){drugsTab.isDisplayed()}
		drugsTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}
}
