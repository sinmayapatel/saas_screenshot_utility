package com.connecture.qa.saas.ifp.utils
import java.io.File
import java.time.LocalDate
import java.time.*
import org.apache.commons.io.filefilter.AgeFileFilter
import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.AgeFileFilter
import static org.apache.commons.io.filefilter.TrueFileFilter.TRUE
import org.apache.commons.io.filefilter.FileFilterUtils
import java.time.Instant
import java.util.Date
import java.sql.Date.*

class DeleteOlderFiles {
	
	public void deleteOlderFiles(noOfDays,pathDirectory){
		def path = new File(pathDirectory)
		if (path.exists()) {
			File[] files = path.listFiles()
			for (int i = 0; i < files.length; i++) {
				long diff = new Date().getTime() - files[i].lastModified()
				def days = (noOfDays.toInteger() * 24 * 60 * 60 * 1000)
				if (diff > days) {
					 files[i].delete()
				/*}	
				else{
					break*/
				} 
			}
		}
	}
	
	
	public void  deleteRecursive(noOfDays,pathDirectory){
		def path = new File(pathDirectory)
		File[] files = path.listFiles()
    System.out.println("Cleaning out folder:" + path.toString())
     for (int i = 0; i < files.length; i++){
		 long diff = new Date().getTime() - files[i].lastModified()
		 def days = (noOfDays.toInteger() * 24 * 60 * 60 * 1000)
        if (path.isDirectory() || diff > days){
			    System.out.println("Deleting file:" + path.toString())
               deleteRecursive(noOfDays,pathDirectory)
                files[i].delete()
             } else {
              files[i].delete()
         }
    }
     path.delete()
} 
	
	
	
    public void DeleteFiles(noOfDays,pathDirectory) {
		LocalDate today = LocalDate.now()
		LocalDate earlier = today.minusDays(noOfDays.toInteger())
		
	    ZoneId defaultZoneId = ZoneId.systemDefault()
		
		Instant instant = earlier.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
		
		
		
        Date threshold = Date.from(instant)
		AgeFileFilter filter = new AgeFileFilter(threshold)
		
        File path = new File(pathDirectory)
		File[] oldFolders = FileFilterUtils.filter(filter, path)
		
		for (File folder : oldFolders) {
			
			System.out.println(folder)
		}
      }
	
	
	void deleteDir(File file) {
		File[] contents = file.listFiles()
		
		if (contents != null) {
			for (File f : contents) {
				long diff = new Date().getTime() - contents[i].lastModified()
				def days = (noOfDays.toInteger() * 24 * 60 * 60 * 1000)
				deleteDir(f);
			}
		}
		file.delete();
	}
}

