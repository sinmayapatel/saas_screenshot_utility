package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCHomePage extends ConnecturePage{
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/Home"
	
	static content = {
		userNameTextField(wait:true){$("input[id='AgentLoginData_UserName']")[0]}
		passwordTextField(wait:true){$("input[id='AgentLoginData_Password']")[0]}
		loginBtn(wait:true){$("a[id='AgentLogin_LoginButton']")[0]}
	}
	
	static at = {
		userNameTextField.isDisplayed()
		passwordTextField.isDisplayed()
	}
	
	def "User enters username and password"(loginCredentials){
		userNameTextField << loginCredentials.userName
		passwordTextField << loginCredentials.password
		
	}
	
	def "User clicks on Login button"(){
		waitFor(){loginBtn.isDisplayed()}
		loginBtn.click()
	}
	
	def "User takes screenshot"(fileDir,pageRequired,currentSpecName){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(userNameTextField.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(loginBtn.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,fileDir,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}
}


