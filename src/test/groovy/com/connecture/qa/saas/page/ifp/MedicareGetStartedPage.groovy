package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

import geb.module.*

class MedicareGetStartedPage extends ConnecturePage {
	
	static url = "PlanCompare/Consumer/Type1/2017/Compare/GetStarted"
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static content = {
		whatIsYourZipCode(wait:true){$("input[name='DemographicsDataModel.ZipCode']")[0].module(TextInput)}
		continueBtn(wait:true){$("a[class='btn btn-primary next leftBlk nextBtn']")[0]}
	}
	
	static at = {
		//whatIsYourZipCode.isDisplayed()
		continueBtn.isDisplayed()
	}
	
	def "User clicks on Continue btn"(){
		waitFor(){continueBtn.isDisplayed()}
		continueBtn.click()
	}
	
	def "User enters Zip code"(zipCode){
		//waitFor(){whatIsYourZipCode.isDisplayed()}
		whatIsYourZipCode.text = zipCode
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(whatIsYourZipCode.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(continueBtn.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}
	
	}else{
		
		}
}}

