package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PConfirmationOfBenefitsPage extends ConnecturePage{
	static url = "Compare/CoverageOptions"
	
	static content = {
		confirmationOfBenefitsText(wait:true){$("div[class='drx-widget TextArea']")[0]}
		returnToAccntOverViewBtn(wait:true){$("a[title='Return to account overview']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.DentalPageTitle
		   waitFor(){copyRightText.isDisplayed()}
	 }
	
	

	def "User verify Confirmation Page & click Return to Accnt"() {
		waitFor(){confirmationOfBenefitsText.isDisplayed()}
		returnToAccntOverViewBtn.click()
	}
	
		
}
