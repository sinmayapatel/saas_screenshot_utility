package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCComparePlansPage extends ConnecturePage{
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/ComparePlans"
	
	static content = {
		viewAndComaprePlans(wait:true){$('h1')[0]}
	}

	static at = {
		viewAndComaprePlans.isDisplayed()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}
}

