package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PMedicalPlanListPage extends ConnecturePage{
	static url = "Compare/CoverageOptions"
	
	static content = {
		viewYourCartTab(wait:true){$("a[data-tabname='View Your Cart']")}
		continueToEnrollmentBtn(wait:true){$("a[id='autonav_Dental']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		dentalTab(wait:true){$("ul[class='coverageGuideTab']>li:nth-child(2)>a")}
		continueShoppingBtn(wait: true){$("a[class='btn coverageGuide_shopBtn btnPadding']")}
		addToCartBtn(wait : true){$("a[class='btn_overload btn_cart btn1']")[2]}
		declineMedicalBtn(wait:true){$("a[title='Decline medical']")}
		cancelWaiverBtn(wait:true){$("a[title='Cancel Waiver']")}
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.MedicalPageTitle
		   copyRightText.isDisplayed()
	 }
	
	

	def "User clicks on add to Cart Btn"(){
		addToCartBtn.click()
	}
	
	def "User clicks on the dental tab"(){
		waitFor(){copyRightText.isDisplayed()}
		waitFor(){dentalTab.isDisplayed()}
		dentalTab.click()
	}
	
	def "User clicks on the Continue Shopping Btn"(){
		waitFor(){continueShoppingBtn.isDisplayed()}
		continueShoppingBtn.click()
	}
	
	def "User checks if medical plan is added then removes it"(){
		if(declineMedicalBtn.isDisplayed())
		{
		declineMedicalBtn.click()
		waitFor(){cancelWaiverBtn.isDisplayed()}
		cancelWaiverBtn.click()
		}
		else if(cancelWaiverBtn.isDisplayed())
		{
		waitFor(){cancelWaiverBtn.isDisplayed()}
		cancelWaiverBtn.click()
		} 
	}		
}
