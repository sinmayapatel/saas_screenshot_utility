package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PEmployeeEnrollmentFormPage extends ConnecturePage{
	static url = "Enroll/EnrollmentForm/Page1"
	
	static content = {
		socialSecurityNumberTextbox(wait:true){$("input[id='Input_29.0']")}
		employeePhoneNumberTextbox(wait:true){$("input[id='Input_16']")}
		addressLine1Textbox(wait:true){$("input[id='Input_20']")}
		cityTextbox(wait:true){$("input[id='Input_22']")}
		stateDropDown(wait:true){$("select[id='DropDown_24']").module(Select)}
		zipCodeTextbox(wait:true){$("input[id='ZipCounty_26']")}
		iAmThePersonCheckBox(wait:true){$("input[id='RadioButton_285_0']")}
		agreeTermsAndConditionsCheckBox(wait:true){$("input[id='CheckBox_214_0']")}
		firstNameText1(wait:true){$("input[id='Input_131']")}
		lastNameText1(wait:true){$("input[id='Input_217']")}
		firstNameText2(wait:true){$("input[id='Input_222']")}
		lastNameText2(wait:true){$("input[id='Input_224']")}
		submitBtn(wait:true){$("input[id='Input_123']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.EnrollmentFormPageTitle
		   copyRightText.isDisplayed()
	 }
	
	

	def "User enter Personal Information SSN number"(employeeEnrollment) {
		socialSecurityNumberTextbox << ""
		socialSecurityNumberTextbox << employeeEnrollment.socialSecurityNo
	}
	
	def "User enter Employee Information phone number"(employeeEnrollment){
		employeePhoneNumberTextbox << ""
		employeePhoneNumberTextbox << employeeEnrollment.phoneNum
	}
	
	def "User enter Permanent Residence Information"(employeeEnrollment) {
		addressLine1Textbox << ""
		addressLine1Textbox << employeeEnrollment.addressLine1
		cityTextbox << ""
		cityTextbox << employeeEnrollment.city
		stateDropdown.selected = employeeEnrollment.state
		zipCodeTextbox << ""
		zipCodeTextbox << employeeEnrollment.zipcode
	}
	
	def "User select I am the person and enter signature"(employeeEnrollment) {
		waitFor(){iAmThePersonCheckBox.isDisplayed()}
		iAmThePersonCheckBox.click()
		agreeTermsAndConditionsCheckBox.click()
		firstNameText1 << employeeEnrollment.lastName
		lastNameText1 << employeeEnrollment.lastName
		firstNameText2 << employeeEnrollment.lastName
		lastNameText2 << employeeEnrollment.lastName
		submitBtn.click()
	}
}
