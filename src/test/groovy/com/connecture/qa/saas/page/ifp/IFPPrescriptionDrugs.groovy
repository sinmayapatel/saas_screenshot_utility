package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class IFPPrescriptionDrugs extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/CoverageSummary"
	
	static content = {
		continueToPlanPreferences(wait:true){$("a[id='autonav_PlanPreferences_p']")[0]}
		doYouWantToAddPrescriptions(wait:true){$('h1')[0]}
	}
	
	static at = {
		continueToPlanPreferences.isDisplayed()
		doYouWantToAddPrescriptions.isDisplayed()
	}
	
	def "User clicks on Continue to Plan Prefrences button"(){
		waitFor(){continueToPlanPreferences.isDisplayed()}
		continueToPlanPreferences.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		
	if(pageRequired.equalsIgnoreCase("yes")){
		if(continueToPlanPreferences.isDisplayed()) {
			if(driver.currentUrl.toString().contains(url)) {
				if(doYouWantToAddPrescriptions.text().contains("Do you want to add prescriptions?")) {
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
				}
			}
		}else{
		
		}
	
	}
}}
