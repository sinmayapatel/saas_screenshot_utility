package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage
import geb.module.*

class AONHomePage extends ConnecturePage{
	
	static url = "compare/Drug/Account/SelectPlan"
	
	static content = {
		title == "AON - Select Your Plan"
		drxLogo(wait:true){ $("div#PoweredByDrxLogo")}
		selectPlanCheckBox(wait:true){i -> $("tbody[id='plan_results'] > tr", text:contains(i)).find("span[class='CheckBox']").module(Checkbox)}
		contunueButton(wait:true){ $("a#btncontinue")}
	}
	
	static at = {
		drxLogo.isDisplayed()
		continueButton.isDisplayed()
	}
	
	def "User selects a plan"(plan){
		selectPlanCheckBox(plan).check()
		assert selectPlanCheckBox(plan).checked	
	}
	
	def "User selects the continue button"(){
		waitFor(){continueButton.isDisplayed()}
		continueButton.click()
	}
	
}


