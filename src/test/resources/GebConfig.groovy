import org.openqa.selenium.firefox.FirefoxDriver
import com.connecture.qa.java.util.excelparser.ExcelParser
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import geb.report.*
import com.connecture.qa.groovy.util.reporting.*
import org.openqa.selenium.remote.*
import org.openqa.selenium.*
import com.connecture.qa.uhg.harness.*


//Config Url's
System.setProperty("geb.build.iFPShopping",System.getProperty("geb.build.iFPShopping","https://NewIFPWBE2.qa.destinationrx.com/platformIFP/"))
System.setProperty("geb.build.medicare",System.getProperty("geb.build.medicare","https://newuxipc.qa.destinationrx.com/"))
System.setProperty("geb.build.medicarePPC",System.getProperty("geb.build.medicarePPC","https://2017acmeppp_1.qa.destinationrx.com/"))

// Grid config
hydraGridUrl = new URL(System.getProperty("autoqa.hydragrid", "http://cnxbrksgqa01.ct.com:4444/wd/hub"))
System.setProperty("geb.build.env","QA")


System.setProperty("geb.build.ifp.mvp", System.getProperty("geb.build.ifp.mvp", "https://mvpfpplan2205-mvp.qa.destinationrx.com/platformifp/"))
//System.setProperty("geb.build.ifp.demo",System.getProperty("geb.build.ifp.demo","https://NewIFPWBE2.qa.destinationrx.com/platformIFP/"))
System.setProperty("geb.build.ifp.demo1",System.getProperty("geb.build.ifp.demo1","https://NewIFPWBE2.qa.destinationrx.com/platformIFP/"))
System.setProperty("geb.build.ifp.demo2",System.getProperty("geb.build.ifp.demo2","https://NewIFPWBE2.qa.destinationrx.com/platformIFP/"))
System.setProperty("geb.build.medicare.demo",System.getProperty("geb.build.medicare.demo","https://newuxipc.qa.destinationrx.com/"))

//DC5_regeression_Production
//System.setProperty("geb.build.DC5RegressionProduction",System.getProperty("geb.build.DC5RegressionProduction","http://aonhewitt.destinationrx.com/Compare/Drug/StartSession"+(char)63+"plans=02233|2020_348776|2016,02233|12973_348741|2016"))

System.setProperty("geb.build.medicareppc.demo",System.getProperty("geb.build.medicareppc.demo","https://2017acmeppp_1.qa.destinationrx.com/"))
// GebConfig file
System.setProperty("client.info.file", System.getProperty("client.info.file", "src/test/groovy/com/connecture/qa/saas/config/ClientConfiguration.groovy"))

// Extended GebConfig file for Stage/Prod passwords
System.setProperty("extendedgebconfig.info.file", System.getProperty("extendedgebconfig.info.file", "/LocalMachineATMConfigs.groovy"))

// Set enviroment level
System.setProperty("geb.build.env", System.getProperty("geb.build.env", "QA")) // QA or STAGE or PROD

// Set Group Name
System.setProperty("geb.build.GroupName", System.getProperty("geb.build.GroupName", "Manual")) //If Manual in spec, this not jenkins build

//Variables for HealthNet Utilities
//Set Employee to enroll
System.setProperty("geb.build.employee", System.getProperty("geb.build.employee", "0"))
//Set rating option (HeathNet only)
System.setProperty("geb.build.RatingMethod", System.getProperty("geb.build.RatingMethod", "Composite Rating")) //Age Rating or Composite Rating
//Set QA environment for HealthNet
System.setProperty("geb.build.HNQA", System.getProperty("geb.build.HNQA", "QA1")) //QA1 or QA2
//Set UAT/STG environment for HealthNet
System.setProperty("geb.build.HNUAT", System.getProperty("geb.build.HNUAT", "UAT1")) //UAT1 or UAT2
//Set census type for HealthNet
System.setProperty("geb.build.CensusType", System.getProperty("geb.build.CensusType", "Complex")) //Small or Large or Cobra or Dependent or Complex
//Set another census flag for HealthNet
System.setProperty("geb.build.CensusActiveOnly", System.getProperty("geb.build.CensusActiveOnly", "false")) //true or false
//Set state for HealthNet
System.setProperty("geb.build.State", System.getProperty("geb.build.State", "AZ")) //AZ or CA or OR or WA
//Set using dental product for HealthNet
System.setProperty("geb.build.Dental", System.getProperty("geb.build.Dental", "true")) //true or false
//Set using vision product for HealthNet
System.setProperty("geb.build.Vision", System.getProperty("geb.build.Vision", "true")) //true or false
//Set using life product for HealthNet
System.setProperty("geb.build.Life", System.getProperty("geb.build.Life", "true")) //true or false
//Set number of medical plans for HealthNet
System.setProperty("geb.build.NumberPlans", System.getProperty("geb.build.NumberPlans", "3")) //integer number 1-4

if (System.getProperty("geb.build.env").equals("QA")) {

	System.setProperty("geb.build.MVP", System.getProperty("geb.build.MVP", "http://mvpatlsgqa03.ct.com/mvp/"))
	System.setProperty("geb.build.BCBSMA", System.getProperty("geb.build.BCBSMA", "http://bmaatlsgqa02.ct.com:8180/bcbsma/"))

	if (System.getProperty("geb.build.HNQA").equals("QA1")) {
		System.setProperty("geb.build.HealthNet", System.getProperty("geb.build.HealthNet", "http://hntatlsgqa01.ct.com:8180/healthnet/"))
	} else {
		System.setProperty("geb.build.HealthNet", System.getProperty("geb.build.HealthNet", "http://hntatlsgqa01.ct.com:8480/healthnet/"))
	}

	System.setProperty("geb.build.MM_MVP", System.getProperty("geb.build.MM_MVP", "https://mvpgroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))
	System.setProperty("geb.build.MM_HealthNet", System.getProperty("geb.build.MM_HealthNet", "https://healthnetgroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))
	System.setProperty("geb.build.MM_Exemplar", System.getProperty("geb.build.MM_Exemplar", "https://qagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))
	System.setProperty("geb.build.MM_Tufts", System.getProperty("geb.build.MM_Tufts", "https://2015pxmmtufts.qa.destinationrx.com/PX_MemberManagement/Group/"))
	System.setProperty("geb.build.MM_BCBSMA", System.getProperty("geb.build.MM_BCBSMA", "https://bcbsmagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))

	System.setProperty("geb.build.ESB_ExternalGroupLoad", System.getProperty("geb.build.ESB_ExternalGroupLoad", "cnxwksftp.connecture.com"))
	System.setProperty("geb.build.ESB_InternalGroupLoad", System.getProperty("geb.build.ESB_InternalGroupLoad", "http://10.1.2.37:9000/integration/"))

	System.setProperty("geb.build.Platform_API", System.getProperty("geb.build.Platform_API", "https://qa.drxwebservices.com/Platform/v1/"))
	System.setProperty("geb.build.Platform_API_AuthServer", System.getProperty("geb.build.Platform_API_AuthServer", "http://qa.destinationrx.com/AuthorizationServer/v1/"))

	System.setProperty("geb.build.PXQCensus", System.getProperty("geb.build.PXQCensus", "src/test/resources/PXQCensusUpload.xls"))
	System.setProperty("geb.build.IACensus", System.getProperty("geb.build.IACensus", "src/test/resources/HealthNetCensusSimple.xls"))


} else if  (System.getProperty("geb.build.env").equals("STAGE")) {

	System.setProperty("geb.build.BCBSMA", System.getProperty("geb.build.BCBSMA", "https://bmasgusgaa01.healthinsurance-asp.com/bcbsma/"))
	System.setProperty("geb.build.MVP", System.getProperty("geb.build.MVP", "https://mvpuat05.healthinsurance-asp.com/mvp/"))
	if (System.getProperty("geb.build.HNUAT").equals("UAT1")) {
		System.setProperty("geb.build.HealthNet", System.getProperty("geb.build.HealthNet", "https://hntuat01.healthinsurance-asp.com/healthnet/"))
	} else {
		System.setProperty("geb.build.HealthNet", System.getProperty("geb.build.HealthNet", "https://hntuat02.healthinsurance-asp.com/healthnet/"))
	}

	System.setProperty("geb.build.MM_MVP", System.getProperty("geb.build.MM_MVP", "https://mvp.staging.cnxrmarketplace.com/Portal/IA/7x/Group/"))
	if (System.getProperty("geb.build.HNUAT").equals("UAT1")) {
		System.setProperty("geb.build.MM_HealthNet", System.getProperty("geb.build.MM_HealthNet", "https://healthnet.staging.cnxrmarketplace.com/Portal/IA/7x/Group/"))
	} else {
		System.setProperty("geb.build.MM_HealthNet", System.getProperty("geb.build.MM_HealthNet", "https://healthnetuat2groupportal.staging.destinationrx.com/MemberManagement/Group/"))
	}
	System.setProperty("geb.build.MM_Exemplar", System.getProperty("geb.build.MM_Exemplar", "https://qagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))
	System.setProperty("geb.build.MM_Tufts", System.getProperty("geb.build.MM_Tufts", "https://tuftsgroupportal.staging.destinationrx.com/MemberManagement/Group/"))
	System.setProperty("geb.build.MM_BCBSMA", System.getProperty("geb.build.MM_BCBSMA", "https://bcbsma.staging.cnxrmarketplace.com/Portal/IA/7x/Group/"))

	System.setProperty("geb.build.ESB_ExternalGroupLoad", System.getProperty("geb.build.ESB_ExternalGroupLoad", "cnxwksftp.connecture.com"))
	System.setProperty("geb.build.ESB_InternalGroupLoad", System.getProperty("geb.build.ESB_InternalGroupLoad", "https://cnxesbloadB1group-internal.cnxprod.com/stage/v14/integration/"))

	System.setProperty("geb.build.Platform_API", System.getProperty("geb.build.Platform_API", "http://staging.drxwebservices.com/Platform/v1/"))
	System.setProperty("geb.build.Platform_API_AuthServer", System.getProperty("geb.build.Platform_API_AuthServer", "https://stage-auth.drxwebservices.com/v1/"))

	System.setProperty("geb.build.PXQCensus", System.getProperty("geb.build.PXQCensus", "src/test/resources/PXQCensusUpload.xls"))
	System.setProperty("geb.build.IACensus", System.getProperty("geb.build.IACensus", "src/test/resources/HealthNetCensusSimple.xls"))

} else if  (System.getProperty("geb.build.env").equals("PROD")) {

	System.setProperty("geb.build.MVP", System.getProperty("geb.build.MVP", "https://mvpuat05.healthinsurance-asp.com/mvp/")) //TODO: GET PROD URL
	System.setProperty("geb.build.HealthNet", System.getProperty("geb.build.HealthNet", "https://hnsg-stg.healthinsurance-asp.com/healthnet/"))
	System.setProperty("geb.build.BCBSMA", System.getProperty("geb.build.BCBSMA", "https://bmasgusgaa01.healthinsurance-asp.com/bcbsma/"))//TODO: GET PROD URL

	System.setProperty("geb.build.MM_MVP", System.getProperty("geb.build.MM_MVP", "https://mvpqatest.cnxrmarketplace.com/Portal/IA/7x/Group/"))
	System.setProperty("geb.build.MM_HealthNet", System.getProperty("geb.build.MM_HealthNet", "https://healthnetqatest.cnxrmarketplace.com/Portal/IA/7x/Group/"))
	System.setProperty("geb.build.MM_Exemplar", System.getProperty("geb.build.MM_Exemplar", "https://qagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/"))//TODO: GET PROD URL
	System.setProperty("geb.build.MM_Tufts", System.getProperty("geb.build.MM_Tufts", "https://tuftsgroupportal.staging.destinationrx.com/MemberManagement/Group/")) //TODO: GET PROD URL
	System.setProperty("geb.build.MM_BCBSMA", System.getProperty("geb.build.MM_BCBSMA", "https://bcbsma.staging.cnxrmarketplace.com/Portal/IA/7x/Group/")) //TODO: GET PROD URL

	System.setProperty("geb.build.ESB_ExternalGroupLoad", System.getProperty("geb.build.ESB_ExternalGroupLoad", "cnxwksftp.connecture.com"))//TODO: GET PROD URL
	System.setProperty("geb.build.ESB_InternalGroupLoad", System.getProperty("geb.build.ESB_InternalGroupLoad", "https://cnxesbloadB1group-internal.cnxprod.com/stage/v14/integration/"))//TODO: GET PROD URL

	System.setProperty("geb.build.Platform_API", System.getProperty("geb.build.Platform_API", "https://www.drxwebservices.com/Platform/v1/"))
	System.setProperty("geb.build.Platform_API_AuthServer", System.getProperty("geb.build.Platform_API_AuthServer", "https://auth.drxwebservices.com:443/v1/"))

	System.setProperty("geb.build.PXQCensus", System.getProperty("geb.build.PXQCensus", "src/test/resources/PRODPXQCensusUpload.xls"))
	System.setProperty("geb.build.IACensus", System.getProperty("geb.build.IACensus", "src/test/resources/HealthNetCensusSimple.xls"))

} else {
	throw new Exception("Environment level could not be found")
}

// MM Site Keys/Ids
if (System.getProperty("geb.build.env").equals("QA")) {

	System.setProperty("geb.build.MVP_AuthUserId", System.getProperty("geb.build.MVP_AuthUserId", "b4f78932-96ef-4954-b05b-ecea37608338"))
	System.setProperty("geb.build.MVP_SiteId", System.getProperty("geb.build.MVP_SiteId", "794fbab6-97ce-e411-80c6-00155d0c5308"))
	System.setProperty("geb.build.MVP_ApiKey", System.getProperty("geb.build.MVP_ApiKey", "kf31ezfhEeWAzwAVXQwqDA"))
	System.setProperty("geb.build.MVP_ApiSecretKey", System.getProperty("geb.build.MVP_ApiSecretKey", "cQCO0rCukdtml9mBHYcnMICAMKwfueRnxHfxaLbU40" ))

	System.setProperty("geb.build.Tufts_AuthUserId", System.getProperty("geb.build.Tufts_AuthUserId", "0c8b8b4f-bd70-4277-9b51-75ad31015ebb"))
	System.setProperty("geb.build.Tufts_SiteId", System.getProperty("geb.build.Tufts_SiteId", "143629e5-3c27-e511-80ce-00155d0c2a0c"))
	System.setProperty("geb.build.Tufts_ApiKey", System.getProperty("geb.build.Tufts_ApiKey", "Z6U3pTfnEeWAzwAVXQwqDA"))
	System.setProperty("geb.build.Tufts_ApiSecretKey", System.getProperty("geb.build.Tufts_ApiSecretKey", "NLKEGqHyNHgSURmfUNusuJA7oSNhXP5y7gStDStPG6g"))

	System.setProperty("geb.build.HealthNet_AuthUserId", System.getProperty("geb.build.HealthNet_AuthUserId", "b4f78932-96ef-4954-b05b-ecea37608338"))
	System.setProperty("geb.build.HealthNet_SiteId", System.getProperty("geb.build.HealthNet_SiteId", "e16916dc-98ce-e411-80c6-00155d0c5308"))
	System.setProperty("geb.build.HealthNet_ApiKey", System.getProperty("geb.build.HealthNet_ApiKey", "fxOT1DfSEeWAzwAVXQwqDA"))
	System.setProperty("geb.build.HealthNet_ApiSecretKey", System.getProperty("geb.build.HealthNet_ApiSecretKey", "CzZ475M3q3xvREo4PyBZ8Vkde5JSlXe5bZX7zNqvL8Y"))

	System.setProperty("geb.build.BCBSMA_AuthUserId", System.getProperty("geb.build.BCBSMA_AuthUserId", "38ed075f-733e-41f2-a378-88bc4cdb6330"))
	System.setProperty("geb.build.BCBSMA_SiteId", System.getProperty("geb.build.BCBSMA_SiteId", "f7497059-b58f-e511-80dd-00155d0c2b30"))
	System.setProperty("geb.build.BCBSMA_ApiKey", System.getProperty("geb.build.BCBSMA_ApiKey", "zBPbn50-EeWA3QAVXQwrMA"))
	System.setProperty("geb.build.BCBSMA_ApiSecretKey", System.getProperty("geb.build.BCBSMA_ApiSecretKey", "r5ivUC5POeu01icUnyjNkUWzJ3Q61NPd2YpiHkB2SaU"))


} else if  (System.getProperty("geb.build.env").equals("STAGE")) {

	System.setProperty("geb.build.MVP_AuthUserId", System.getProperty("geb.build.MVP_AuthUserId", "5bbc63ae-5d24-417a-8c45-65bb98bf1a87"))
	System.setProperty("geb.build.MVP_SiteId", System.getProperty("geb.build.MVP_SiteId", "2f1bbced-e6c8-e411-80cf-00155d0c5106"))
	System.setProperty("geb.build.MVP_ApiKey", System.getProperty("geb.build.MVP_ApiKey", "W0sf8GFSEeWA3QAVXQwpIg"))
	System.setProperty("geb.build.MVP_ApiSecretKey", System.getProperty("geb.build.MVP_ApiSecretKey", "FkJP5dtuwYSIYAQWjjH0kpuSFUfaEdZXRjiOZVNj20"))

	System.setProperty("geb.build.Tufts_AuthUserId", System.getProperty("geb.build.Tufts_AuthUserId", "9f41d897-51bd-4e13-a8ed-3ddf56a8ded4"))
	System.setProperty("geb.build.Tufts_SiteId", System.getProperty("geb.build.Tufts_SiteId", "0d6df8e1-edcc-e411-80cf-00155d0c5106"))
	System.setProperty("geb.build.Tufts_ApiKey", System.getProperty("geb.build.Tufts_ApiKey", "a964X2FSEeWA3QAVXQwpIg"))
	System.setProperty("geb.build.Tufts_ApiSecretKey", System.getProperty("geb.build.Tufts_ApiSecretKey", "FJnA2eB6qM95DhC7dqqB16vzq12oYwhsI1BWVYCY0"))

	System.setProperty("geb.build.HealthNet_AuthUserId", System.getProperty("geb.build.HealthNet_AuthUserId", "5bbc63ae-5d24-417a-8c45-65bb98bf1a87"))
	System.setProperty("geb.build.HealthNet_SiteId", System.getProperty("geb.build.HealthNet_SiteId", "2e1bbced-e6c8-e411-80cf-00155d0c5106"))
	System.setProperty("geb.build.HealthNet_ApiKey", System.getProperty("geb.build.HealthNet_ApiKey", "6X_HmmFHEeWA3QAVXQwpIg"))
	System.setProperty("geb.build.HealthNet_ApiSecretKey", System.getProperty("geb.build.HealthNet_ApiSecretKey", "XZp0mIHVqef5BqUf98Lek5MJvIyWvO3ae23VSOLhZ5w"))

	System.setProperty("geb.build.BCBSMA_AuthUserId", System.getProperty("geb.build.BCBSMA_AuthUserId", "3f6400e0-0253-4e5e-abee-eac45361bac9"))
	System.setProperty("geb.build.BCBSMA_SiteId", System.getProperty("geb.build.BCBSMA_SiteId", "a41ee433-aa8f-e511-80e2-00155d0c282a"))
	System.setProperty("geb.build.BCBSMA_ApiKey", System.getProperty("geb.build.BCBSMA_ApiKey", "3i3FB23OEeWA3wAVXQwoIw"))
	System.setProperty("geb.build.BCBSMA_ApiSecretKey", System.getProperty("geb.build.BCBSMA_ApiSecretKey", "jPVsJGZ37EGLTVtxZBCyi5iNEwt2rTy6PBFLO59xBc"))

} else if  (System.getProperty("geb.build.env").equals("PROD")) {

	// The MVP Prod site is a child site of the true/actual MVP PROD site
	System.setProperty("geb.build.MVP_AuthUserId", System.getProperty("geb.build.MVP_AuthUserId", "493b5fad-9b31-46c2-b453-6d579b08d192"))
	System.setProperty("geb.build.MVP_SiteId", System.getProperty("geb.build.MVP_SiteId", "69c1e3c5-6f4f-e611-80dc-0024e85a4a7d"))
	System.setProperty("geb.build.MVP_ApiKey", System.getProperty("geb.build.MVP_ApiKey", "xlBnoYQCEeWA0QAk6FpKfQ"))
	System.setProperty("geb.build.MVP_ApiSecretKey", System.getProperty("geb.build.MVP_ApiSecretKey", "ZVsAKNkdFUOIPv3OyHmSb6CVHaJ1RUSAIoDoeftZsI"))

	// Healthnet Prod site is child site of true healthnet PROD
	System.setProperty("geb.build.HealthNet_AuthUserId", System.getProperty("geb.build.HealthNet_AuthUserId", "90051ee8-41b8-44a0-86b3-1af436251eb8"))
	System.setProperty("geb.build.HealthNet_SiteId", System.getProperty("geb.build.HealthNet_SiteId", "a05410dd-c397-e611-80e1-0024e85a4a7d"))
	System.setProperty("geb.build.HealthNet_ApiKey", System.getProperty("geb.build.HealthNet_ApiKey", "txc3CTbkEeWAzAAk6FpKdQ"))
	System.setProperty("geb.build.HealthNet_ApiSecretKey", System.getProperty("geb.build.HealthNet_ApiSecretKey", "t3a4AWqqeNqTAdGWpuz53UcZ7URvzO0FJXUUH9X9Up8"))


} else {
	throw new Exception("Env. level is not supported")
}

// Exemplar Site credentials
if (System.getProperty("geb.build.MM_Exemplar").equals("https://qagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/")) {
	System.setProperty("geb.build.MM_Exemplar_username", System.getProperty("geb.build.MM_Exemplar_username","brokertest"))
	System.setProperty("geb.build.MM_Exemplar_password", System.getProperty("geb.build.MM_Exemplar_username", "brokertest"))
}

// MVP Site credentials
if (System.getProperty("geb.build.MVP").equals("http://mvpatlsgqa03.ct.com/mvp/")) {
	System.setProperty("geb.build.MVP_sales_username", System.getProperty("geb.build.MVP_sales_username","sallysales"))
	System.setProperty("geb.build.MVP_broker_username", System.getProperty("geb.build.MVP_broker_username","bobbroker"))
	System.setProperty("geb.build.MVP_password", System.getProperty("geb.build.MVP_password", "password1"))
}

// HealthNet Site credentials
if (System.getProperty("geb.build.HealthNet").equals("http://hntatlsgqa01.ct.com:8180/healthnet/") || System.getProperty("geb.build.HealthNet").equals("http://hntatlsgqa01.ct.com:8480/healthnet/")) {
	System.setProperty("geb.build.HealthNet_broker_username", System.getProperty("geb.build.HealthNet_broker_username","bobbroker"))
	System.setProperty("geb.build.HealthNet_underwriter_username", System.getProperty("geb.build.HealthNet_underwriter_username","orpheusuw"))
	System.setProperty("geb.build.HealthNet_sales_username", System.getProperty("geb.build.HealthNet_sales_username","sallysales"))
	System.setProperty("geb.build.HealthNet_password", System.getProperty("geb.build.HealthNet_password", "CnxrQA2017"))
}

// BCBSMA Site Credentials
if (System.getProperty("geb.build.BCBSMA").equals("http://bmaatlsgqa02.ct.com:8180/bcbsma/")) {
	System.setProperty("geb.build.BCBSMA_broker_username", System.getProperty("geb.build.BCBSMA_broker_username","bobbroker"))
	System.setProperty("geb.build.BCBSMA_sales_username", System.getProperty("geb.build.BCBSMA_sales_username","sallysales"))
	System.setProperty("geb.build.BCBSMA_underwriter_username", System.getProperty("geb.build.BCBSMA_underwriter_username","orpheusuw"))
	System.setProperty("geb.build.BCBSMA_password", System.getProperty("geb.build.BCBSMA_password","CnxrQA2017"))
}

// BCBSMA MM Site credentials
if (System.getProperty("geb.build.MM_BCBSMA").equals("https://bcbsmagroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/")) {
	System.setProperty("geb.build.MM_BCBSMA_username", System.getProperty("geb.build.MM_BCBSMA_username","BCBSMA_PXbrokertest"))
	System.setProperty("geb.build.MM_BCBSMA_password", System.getProperty("geb.build.MM_BCBSMA_password", "BCBSMA_PXbrokertest"))
}

// MVP MM Site credentials
if (System.getProperty("geb.build.MM_MVP").equals("https://mvpgroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/")) {
	System.setProperty("geb.build.MM_MVP_username", System.getProperty("geb.build.MM_MVP_username","CNXInternal2015"))
	System.setProperty("geb.build.MM_MVP_password", System.getProperty("geb.build.MM_MVP_password", "CNXInternal2015!"))
}

// Tufts MM Site credentials
if (System.getProperty("geb.build.MM_Tufts").equals("https://2015pxmmtufts.qa.destinationrx.com/PX_MemberManagement/Group/")) {
	System.setProperty("geb.build.MM_Tufts_username", System.getProperty("geb.build.MM_Tufts_username","Tufts_PXbrokertest"))
	System.setProperty("geb.build.MM_Tufts_password", System.getProperty("geb.build.MM_Tufts_password", "Tufts123"))
}

// HealthNet MM Site credentials
if (System.getProperty("geb.build.MM_HealthNet").equals("https://healthnetgroupagentportal.qa.destinationrx.com/PX_MemberManagement/Group/")) {
	System.setProperty("geb.build.MM_HealthNet_broker_username", System.getProperty("geb.build.MM_HealthNet_broker_username","hnetbroker"))
	System.setProperty("geb.build.MM_HealthNet_underwriter_username", System.getProperty("geb.build.MM_HealthNet_underwriter_username","hnetunderwriter"))
	System.setProperty("geb.build.MM_HealthNet_password", System.getProperty("geb.build.MM_HealthNet_password", "123456"))
}

// ESB External Group Load Credentials
if (System.getProperty("geb.build.ESB_ExternalGroupLoad").equals("cnxwksftp.connecture.com")) {
	System.setProperty("geb.build.ESB_ExternalGroupLoad_username", System.getProperty("geb.build.ESB_ExternalGroupLoad_username", "ESB1M"))
	System.setProperty("geb.build.ESB_ExternalGroupLoad_password", System.getProperty("geb.build.ESB_ExternalGroupLoad_password", "uYPx)V6H"))
} else {
	throw new Exception("Username/passwords could not be found for External ESB service")
}

// ESB Internal Group Load Credentials
if (System.getProperty("geb.build.ESB_InternalGroupLoad").equals("http://10.1.2.37:9000/integration/")) {
	System.setProperty("geb.build.ESB_InternalGroupLoad_username", System.getProperty("geb.build.ESB_InternalGroupLoad_username", "qauser"))
	System.setProperty("geb.build.ESB_InternalGroupLoad_password", System.getProperty("geb.build.ESB_InternalGroupLoad_password", "qapassword"))
} else if (System.getProperty("geb.build.ESB_InternalGroupLoad").equals("https://cnxesbloadB1group-internal.cnxprod.com/stage/v14/integration/")) {
	System.setProperty("geb.build.ESB_InternalGroupLoad_username", System.getProperty("geb.build.ESB_InternalGroupLoad_username", "onemarketplacestgservice"))
	System.setProperty("geb.build.ESB_InternalGroupLoad_password", System.getProperty("geb.build.ESB_InternalGroupLoad_password", "qUXbew8k"))
} else {
	throw new Exception("Username/passwords could not be found for Internal ESB service")
}

driver = {
	System.setProperty("webdriver.chrome.driver", System.getProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe"))
	ChromeOptions options = new ChromeOptions()
	DesiredCapabilities capabilities = DesiredCapabilities.chrome()
	capabilities.setCapability(ChromeOptions.CAPABILITY, options)
	def driver = new ChromeDriver(capabilities)

}

environments {
	"ie" {
		driver = {
			System.setProperty("webdriver.ie.driver", System.getProperty("webdriver.ie.driver", "./BrowserDrivers/IEDriverServer.exe"))
			def ieCapabilities = DesiredCapabilities.internetExplorer()
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true)
			def driver = new InternetExplorerDriver(ieCapabilities)
			driver.manage().window().maximize()
			return driver
		}
	}
	"chrome"{
		driver = {
			System.setProperty("webdriver.chrome.driver", System.getProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe"))
			ChromeOptions options = new ChromeOptions()
			DesiredCapabilities capabilities = DesiredCapabilities.chrome()

			Map<String, Object> prefs = new HashMap<String, Object>();
			def allowOption = 1
			prefs.put("profile.managed_default_content_settings.geolocation", allowOption);
			options.setExperimentalOption("prefs", prefs);

			options.addArguments("no-sandbox", "unexpectedAlertBehaviour=dismiss", "test-type")
			capabilities.setCapability(ChromeOptions.CAPABILITY, options)

			def driver = new ChromeDriver(capabilities)
			driver.manage().window().maximize()
			return driver
		}
	}
	"firefox"{
		driver = {
			def driver = new FirefoxDriver()
			driver.manage().window().maximize()
			return driver
		}
	}
	"remote-chrome" {
		driver = {
			System.setProperty("webdriver.chrome.driver", System.getProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe"))
			ChromeOptions options = new ChromeOptions()
			DesiredCapabilities capabilities = DesiredCapabilities.chrome()

			Map<String, Object> prefs = new HashMap<String, Object>();
			def allowOption = 1
			prefs.put("profile.managed_default_content_settings.geolocation", allowOption);
			options.setExperimentalOption("prefs", prefs);

			options.addArguments("no-sandbox", "unexpectedAlertBehaviour=dismiss", "test-type")
			capabilities.setCapability(ChromeOptions.CAPABILITY, options)
			def driver = new RemoteWebDriver(hubUrl, capabilities)
			driver.manage().window().maximize()
			return driver
		}
	}
	'hydragrid-chrome' {
		driver = {
			ChromeOptions options = new ChromeOptions();
			def targetDirectoryPath =  System.getProperty("user.dir") + File.separator + "target"
			def chromePreferences = new HashMap<String, Object>()
			chromePreferences.put("download.directory_upgrade", "true")
			chromePreferences.put("download.default_directory", targetDirectoryPath)
			chromePreferences.put("download.prompt_for_download", "false")
			options.setExperimentalOption("prefs", chromePreferences)
			options.addArguments("--start-maximized");
			options.addArguments("--no-sandbox");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			def driver = new RemoteWebDriver(hydraGridUrl, capabilities)
			driver.setFileDetector(new LocalFileDetector())
			return driver
		}
	}
}

reportsDir = System.getProperty("geb.build.reportsDir", "./target/geb-screenshots-and-reports")

atCheckWaiting = true

waiting {
	timeout = 300 // Increasing for RDE submission in QA
	retryInterval = 0.5
}


reporter = new CompositeReporter(new PageSourceReporter(), new ScreenshotReporter(), new DesktopScreenshotReporter(), new ChromeLogReporter())
reportingListener = new JenkinsReportingListener()

/*
 This is the Geb configuration file.
 See: http://www.gebish.org/manual/current/configuration.html
 */