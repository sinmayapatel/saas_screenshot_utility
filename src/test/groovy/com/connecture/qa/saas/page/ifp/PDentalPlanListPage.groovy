package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PDentalPlanListPage extends ConnecturePage{
	static url = "Compare/CoverageOptions"
	
	static content = {
		viewYourCartTab(wait:true){$("a[data-tabname='Dental']")}
		continueToViewCartBtn(wait:true){$("a[id='autonav_BenefitSummary']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		declineDentalBtn(wait:true){$("a[title='Decline dental']")}
		selectReasonDeclineDropDown(wait:true){$("div[id='CoverageGuideWaivingCoverage']>select[class='CoverageGuideWaivingReasons']").module(Select)}
		continueShoppingBtn(wait: true){$("a[class='btn coverageGuide_shopBtn btnPadding']")}
		addToCartBtn(wait : true){$("a[class='btn_overload btn_cart btn1']")[2]}
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.DentalPageTitle
		   copyRightText.isDisplayed()
	 }
	
	

	def "User click continue to Cart Btn"() {
		waitFor(){continueToViewCartBtn.isDisplayed()}
		continueToViewCartBtn.click()
	}
	
	def "User click Decline Dental Btn"() {
		waitFor(){declineDentalBtn.isDisplayed()}
		declineDentalBtn.click()
	}
	
	def "User select Decline Reason from dropdown"(declineReason) {
		selectReasonDeclineDropDown.selected = declineReason
		assert selectReasonDeclineDropDown.selectedText == declineReason
	}
	
	def "User clicks on the Continue Shopping Btn"(){
		waitFor(){continueShoppingBtn.isDisplayed()}
		continueShoppingBtn.click()
	}
		
	def "User clicks on add to Cart Btn"(){
		addToCartBtn.click()
	}
}
