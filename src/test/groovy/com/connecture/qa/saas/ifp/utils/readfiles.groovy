package com.connecture.qa.saas.ifp.utils


import java.io.File
import com.connecture.qa.java.util.excelparser.ExcelParser
import org.apache.commons.io.FileUtils
import com.connecture.qa.config.ConnectureReportingSpecification

import com.connecture.qa.java.util.mail.*
import com.connecture.qa.uhg.harness.*
import com.connecture.qa.uhg.page.*
import com.google.common.collect.Count
import com.connecture.qa.java.util.fileutils.Sftp
import com.connecture.qa.java.util.fileutils.Ftp
import com.connecture.qa.java.util.exception.ConnectureCustomException
import com.connecture.qa.java.util.fileutils.Sftp
import com.connecture.qa.java.util.fileutils.Ftp
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil
import java.text.DateFormat
import java.text.SimpleDateFormat
import org.codehaus.groovy.runtime.*
import org.apache.commons.io.FileUtils
import java.io.File;


class readfiles extends ConnectureReportingSpecification{

	public readFromMultipleExcel() {
		def source = new File("//172.16.138.24/UHG-Docs/Automation/ScreenShotUtility/WorkOriginal")
		def dest = new File("//172.16.138.24/UHG-Docs/Automation/ScreenShotUtility/WorkCopy")
		for (int i=0; i<=(3-(dest.list().size())); i++) {
			FileUtils.copyFileToDirectory(source.listFiles()[i], dest)
		}
		File[] allSubFiles = dest.listFiles()
		def lmp = []
		def count =allSubFiles.length
		for (File file : allSubFiles) {
			int i=2
			def mp1 = [:]
			ExcelParser parser = new ExcelParser(file.getPath())
			mp1.put(parser.getCellData(1,5),parser.getCellData(2,5))
			mp1.put(parser.getCellData(1,3),parser.getCellData(2,3))
			for( i; i<=parser.getCountOfFilledRows(); i++) {
				mp1.put(parser.getCellData(i,1), parser.getCellData(i,2))
			}
			lmp.push(mp1)
			file.delete()
		}

		return lmp
	}
}