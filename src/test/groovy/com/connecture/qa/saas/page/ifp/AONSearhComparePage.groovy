package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class AONSearhComparePage extends ConnecturePage{
	
	static url = "compare/drug/Home/Home"
	
	static content = {
		title == "AON - Search , compare , and save on prescription drugs"
		drxLogo(wait:true)${("div#PoweredByDrxLogo")}
	}
	
	static at = {
		driver.currentUrl.toString().contains(url)
		drxLogo.isDisplayed()
	}
	
}


