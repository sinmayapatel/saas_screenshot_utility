package com.connecture.qa.saas.ifp.lsa;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage
import java.awt.image.PixelGrabber;
import static org.junit.Assert.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.net.URL;

import org.junit.Test;
public class ImageCompare {

	public compare_images(file1,file2) {
		Image image1 = Toolkit.getDefaultToolkit().getImage(file1.toString());
		Image image2 = Toolkit.getDefaultToolkit().getImage(file2.toString());
		int[] data1 = null;
		int[] data2 = null;
		try {
			PixelGrabber grab1 = new PixelGrabber(image1, 0, 0, -1, -1, false);
			PixelGrabber grab2 = new PixelGrabber(image2, 0, 0, -1, -1, false);

			// int[] data1 = null;

			if (grab1.grabPixels())
			{
				int width = grab1.getWidth();
				int height = grab1.getHeight();
				data1 = new int[width * height];
				data1 = (int[]) grab1.getPixels();
			}

			//int[] data2 = null;

			if (grab2.grabPixels()) {
				int width = grab2.getWidth();
				int height = grab2.getHeight();
				data2 = new int[width * height];
				data2 = (int[]) grab2.getPixels();
			}

			//println ("Images are same: " + java.util.Arrays.equals(data1, data2))
			return java.util.Arrays.equals(data1, data2)
			/*def diff_percent = (data1 / data2) * 100
			 if(diff_percent > 10.00){
			 return false
			 }else{
			 return true
			 }*/
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}


}


