package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCGetStartedPage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/GetStarted"
	
	static content = {
		healthInformationText(wait:true){$('h1')[0]}
		subsidyTab(wait:true){$("a[title='Step 4 of 9 is for Subsidy']")[0]}
	}
	
	static at = {
		healthInformationText.isDisplayed()
	}
	
	def "User clicks on Subsidy tab"(){
		waitFor(){subsidyTab.isDisplayed()}
		subsidyTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}

}
