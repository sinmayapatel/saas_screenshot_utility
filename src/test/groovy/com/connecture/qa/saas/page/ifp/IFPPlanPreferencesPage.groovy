package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper
class IFPPlanPreferencesPage extends ConnecturePage {
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/CoverageSummary"
	
	static content = {
		continueToNextQuestion(wait:true){$("a[class='btn btn1 right pref_next']")[0]}
		planPreferences(wait:true){$('h1')[0]}
		summaryTab(wait:true){$("a[data-producttype='Summary']")[0]}
	}

	static at = {
		continueToNextQuestion.isDisplayed()
		planPreferences.isDisplayed()
	}
	
	def "User clicks on Summary tab"(){
		waitFor(){summaryTab.isDisplayed()}
		summaryTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		
	if(pageRequired.equalsIgnoreCase("yes")){
		if(continueToNextQuestion.isDisplayed()) {
			if(driver.currentUrl.toString().contains(url)) {
				if(planPreferences.text().contains("Plan Preferences")) {
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
				}
			}
		}else{
		
		}
	
	}
}}
