package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicareAddDrugsPage extends ConnecturePage {
	
	static url = "PlanCompare/Consumer/Type1/2017/Compare/AddDrugs"
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static content = {
		addDrugsText(wait:true){$('h1')[0]}
		skipBtn(wait:true){$("a[title='Skip this Step']")[0]}
	}
	
	static at = {
		addDrugsText.isDisplayed()
		skipBtn.isDisplayed()
	}
	
	def "User clicks on Skip button"(){
		waitFor(){skipBtn.isDisplayed()}
		skipBtn.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes"))
		{
			if(addDrugsText.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(skipBtn.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}
	
	}else{
		
		}
}}
