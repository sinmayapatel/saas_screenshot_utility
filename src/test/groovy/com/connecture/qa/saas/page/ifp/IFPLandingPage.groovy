package com.connecture.qa.saas.page.ifp

import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper


class IFPLandingPage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	static url = "Compare/Home"
	
	ScreenShotHelper h= new ScreenShotHelper()
	
	static content = {
		youCanViewPlansWithoutEnrolling(wait:true){$('a.link')}
		logo(wait:true){$('#siteLogo')}
	}

	static at = { youCanViewPlansWithoutEnrolling.isDisplayed() }

	def "User click on youCanViewPlansWithoutEnrolling link"(){
		youCanViewPlansWithoutEnrolling.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,currentSpecName){
			
		if(pageRequired.equalsIgnoreCase("yes")){
			if(youCanViewPlansWithoutEnrolling.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(logo.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}
}

