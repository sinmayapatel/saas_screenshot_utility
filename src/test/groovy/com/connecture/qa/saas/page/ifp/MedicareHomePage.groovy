package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicareHomePage extends ConnecturePage{
	
	static url = "PlanCompare/Consumer/Type1/2017/Compare/Home"
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static content = {
		getStarted(wait:true){$("a[class='btn-primary get-started-btn']")[1]}
		siteLogo(wait:true){$('#siteLogo')[0]}
	}
	
	static at = {
		getStarted.isDisplayed()
		siteLogo.isDisplayed()
	}
	
	def "User clicks on GetStarted btn"(){
		waitFor(){getStarted.isDisplayed()}
		getStarted.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(getStarted.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(siteLogo.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}
	
	}else{
		
		}
	}}
