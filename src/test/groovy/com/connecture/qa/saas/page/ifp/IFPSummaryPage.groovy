package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class IFPSummaryPage extends ConnecturePage {
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/CoverageSummary"
	
	static content = {
		pickAPlan(wait:true){$("a[class='btn btn1']")[1]}
		summary(wait:true){$('h1')[0]}
	}
	
	static at = {
		pickAPlan.isDisplayed()
		summary.isDisplayed()
	}
	
	def "User clicks on Pick A Plan button"(){
		waitFor(){pickAPlan.isDisplayed()}
		pickAPlan.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		
	if(pageRequired.equalsIgnoreCase("yes")){
		if(pickAPlan.isDisplayed()) {
			if(driver.currentUrl.toString().contains(url)) {
				if(summary.text().contains("Summary")) {
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
				}
			}
		}else{
		
		}
	
	}
}}
