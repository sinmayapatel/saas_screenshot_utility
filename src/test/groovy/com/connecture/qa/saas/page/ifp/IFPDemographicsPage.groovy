package com.connecture.qa.saas.page.ifp

import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class IFPDemographicsPage extends ConnecturePage {
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/Demographics"
	
	static content = {		
		zipCode(wait:true){$('#DemographicsDataModel_ZipCode')}
		firstName(wait:true){$('#MemberProfileDataModel_FirstName')}
		lastName(wait:true){$('#MemberProfileDataModel_LastName')}
		DOB(wait:true){$('#MemberProfileDataModel_DateOfBirth')}
		continueBtn(wait:true){$('a.nextBtn')}
		gender(wait:true){$("[for='MemberProfileDataModel1']")}
		completeYourProfile(wait:true){$('h1')}		
		}
	
	static at = {
		zipCode.isDisplayed ()
		continueBtn.isDisplayed()
		}
	
	def "User Enters Demographic Details"(demographicDetails){
		zipCode << demographicDetails.ZipCode
		firstName << demographicDetails.FirstName
		lastName << demographicDetails.LastName
		DOB << demographicDetails.DOB
		}
	
	def "User Clicks On Coninue Button"(){
		gender.click()
		continueBtn.click()
		}
	
	def "User takes screenshot"(specName,pageRequired,currentSpecName){
			
			if(pageRequired.equalsIgnoreCase("yes")){
			if(zipCode.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(firstName.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,currentSpecName)
					}
				}
			}
		}else{
		
		}
	
	}
}

