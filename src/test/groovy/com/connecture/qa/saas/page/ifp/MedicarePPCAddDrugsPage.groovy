package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCAddDrugsPage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/AddDrugs"
	
	static content = {
		addDrugsText(wait:true){$('h2')[0]}
		pharmacyTab(wait:true){$("a[title='Step 6 of 9 is for Pharmacy']")[0]}
	}
	
	static at = {
		addDrugsText.isDisplayed()
	}
	
	def "User clicks on Pharmacy tab"(){
		waitFor(){pharmacyTab.isDisplayed()}
		pharmacyTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}

}
