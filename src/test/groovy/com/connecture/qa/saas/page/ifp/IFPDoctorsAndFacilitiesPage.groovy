package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper
class IFPDoctorsAndFacilitiesPage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/CoverageSummary"
	
	static content = {
		continueToPrescriptionDrugs(wait:true){$("a[id='autonav_PrescriptionDrugs_p']")[0]}
		doctorsandFacilities(wait:true){$('h1')[0]}
	}
	
	static at = {
		continueToPrescriptionDrugs.isDisplayed()
		doctorsandFacilities.isDisplayed()
	}
	
	def "User clicks on Continue to prescription drugs"(){
		waitFor(){continueToPrescriptionDrugs.isDisplayed()}
		continueToPrescriptionDrugs.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes"))
		{
			if(continueToPrescriptionDrugs.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(doctorsandFacilities.text().contains("Doctors and facilities")) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}else{
		
		}
	
	}
}}
