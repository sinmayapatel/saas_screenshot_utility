package com.connecture.qa.saas.ifp.utils;

import geb.report.*

import java.awt.*
import java.awt.image.BufferedImage

import javax.imageio.ImageIO

import org.apache.commons.io.FileUtils
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.chrome.ChromeDriver

import ru.yandex.qatools.ashot.AShot
import ru.yandex.qatools.ashot.Screenshot
import ru.yandex.qatools.ashot.shooting.ShootingStrategies

import java.io.File
import java.io.FileOutputStream
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

import java.io.ByteArrayOutputStream

public class ScreenShotHelper extends ScreenshotReporter {


	public void writeReport(driver,browser,label,outputDir) {
		def fullPageScreenshotFile = getFile(outputDir, label + "-chromeFullPage", "png")
		ImageIO.write(takeScreenshot(driver,browser,label,outputDir), "png", fullPageScreenshotFile)
	}
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
	Date date = new Date()
	def currentDateTime = dateFormat.format(date).toString().replaceAll(  "/","-").replaceAll(" ","_").replaceAll(":",".")

	public takeScreenshotSinglePage(driver,fileDir,pageName,currentSpecName){
		def screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(10000)).takeScreenshot(driver)
		def saveDir = fileDir + pageName+ "\\" +pageName + ".png"
		File outputFile = new File(saveDir)
		outputFile.mkdirs()
		ImageIO.write(screenshot.getImage(), "png",outputFile)
		return outputFile
	}
}