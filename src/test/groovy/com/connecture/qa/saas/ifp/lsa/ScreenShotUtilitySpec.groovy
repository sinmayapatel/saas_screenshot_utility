package com.connecture.qa.saas.ifp.lsa

import com.connecture.qa.config.ConnectureReportingSpecification
import com.connecture.qa.saas.config.SAASReportingSpec;
import com.connecture.qa.saas.page.ifp.*

import spock.lang.*
import groovy.transform.Field

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date;

import javassist.bytecode.stackmap.BasicBlock.Catch;

import com.connecture.qa.java.util.excelparser.ExcelParser
import com.connecture.qa.saas.ifp.utils.*

import geb.Page

class ScreenShotUtilitySpec extends ConnectureReportingSpecification {

	def "Verify Cart Page" () {
		switch(req.URL) {
			case(System.getProperty("geb.build.iFPShopping")):
				browser.setBaseUrl(req.URL)
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				Date date = new Date()
				def currentDateTime = dateFormat.format(date).toString().replaceAll(  "/","-").replaceAll(" ","_").replaceAll(":",".")
				def fileDir = location + "\\"  + currentDateTime  + "_" + req.UserEmail.toString().split("@")[0] + "\\"
				def filePath = "file:" + location + "/"  + currentDateTime  + "_" + req.UserEmail.toString().split("@")[0] + "/"
				def imgcomp = new ImageCompare()
				def email = new EmailUtility()
				def statusFileLocation = fileDir + "\\" + "StatusReport.txt"
				File statusFile = new File(statusFileLocation)
				if(!statusFile.exists()) {
					statusFile.getParentFile().mkdirs()
					statusFile.createNewFile()
				}

			when :
				to IFPHomePage
				def baselLineImageHomePage = new File(baseLineLocation + "IFPShopping/IFPHomePage.png")
				if(req.homePage.equalsIgnoreCase("yes")){
					def homePageImage = "User takes screenshot"(fileDir,req.homePage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(homePageImage,baselLineImageHomePage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile, true))
						out.write("IFPHomePage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPHomePage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"Enter Username and Password and Click On AccessSite link"(username,password)

			then:
				at IFPLandingPage

			when:
				def baselLineIFPLandingPage = new File(baseLineLocation + "IFPShopping/IFPLandingPage.png")
				if(req.landingPage.equalsIgnoreCase("yes")){
					def IFPLandingPageImage="User takes screenshot"(fileDir,req.landingPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPLandingPageImage,baselLineIFPLandingPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPLandingPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPLandingPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User click on youCanViewPlansWithoutEnrolling link"()

			then:
				at IFPDemographicsPage

			when:
				def baselLineIFPDemographicsPage = new File(baseLineLocation + "IFPShopping/IFPDemographicsPage.png")
				if(req.demographicsPage.equalsIgnoreCase("yes")){
					def IFPIFPDemographicsPageImage = "User takes screenshot"(fileDir,req.demographicsPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPIFPDemographicsPageImage,baselLineIFPDemographicsPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPDemographicsPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPDemographicsPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User Enters Demographic Details"(demographicDetails)
				"User Clicks On Coninue Button"()

			then:
				at SelectBrokerPage

			when:
				def baselLineSelectBrokerPage = new File(baseLineLocation + "IFPShopping/SelectBrokerPage.png")
				if(req.selectBrokerPage.equalsIgnoreCase("yes")){
					def SelectBrokerPageImage = "User takes screenshot"(fileDir,req.selectBrokerPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(SelectBrokerPageImage,baselLineSelectBrokerPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("SelectBrokerPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("SelectBrokerPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User Select Broker From Drop down"()
				"User clicks on Coninue to Shopping button"()

			then:
				at IFPMethodPage

			when:
				def baselLineIFPMethodPage = new File(baseLineLocation + "IFPShopping/IFPMethodPage.png")
				if(req.IFPMethodPage.equalsIgnoreCase("yes")){
					def IFPMethodPageImage = "User takes screenshot"(fileDir,req.IFPMethodPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPMethodPageImage,baselLineIFPMethodPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPMethodPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPMethodPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Help Me Choose button"()

			then:
				at IFPHealthQuestionsPage

			when:
				def baselLineIFPHealthQuestionsPage = new File(baseLineLocation + "IFPShopping/IFPHealthQuestionsPage.png")
				if(req.IFPHealthQuestionsPage.equalsIgnoreCase("yes")){
					def IFPHealthQuestionsPageImage = "User takes screenshot"(fileDir,req.IFPHealthQuestionsPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPHealthQuestionsPageImage,baselLineIFPHealthQuestionsPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPHealthQuestionsPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPHealthQuestionsPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on 'Continue to Doctors and Facilities button"()

			then:
				at IFPDoctorsAndFacilitiesPage

			when:
				def baselLineIFPDoctorsAndFacilitiesPage = new File(baseLineLocation + "IFPShopping/IFPDoctorsAndFacilitiesPage.png")
				if(req.IFPDoctorsAndFacilitiesPage.equalsIgnoreCase("yes")){
					def IFPDoctorsAndFacilitiesPageImage = "User takes screenshot"(fileDir,req.IFPDoctorsAndFacilitiesPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPDoctorsAndFacilitiesPageImage,baselLineIFPDoctorsAndFacilitiesPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPDoctorsAndFacilitiesPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPDoctorsAndFacilitiesPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Continue to prescription drugs"()

			then:
				at IFPPrescriptionDrugs

			when:
				def baselLineIFPPrescriptionDrugs = new File(baseLineLocation + "IFPShopping/IFPPrescriptionDrugs.png")
				if(req.IFPPrescriptionDrugs.equalsIgnoreCase("yes")){
					def IFPPrescriptionDrugsImage = "User takes screenshot"(fileDir,req.IFPPrescriptionDrugs,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPPrescriptionDrugsImage,baselLineIFPPrescriptionDrugs)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPrescriptionDrugs image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPrescriptionDrugs image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Continue to Plan Prefrences button"()

			then:
				at IFPPlanPreferencesPage

			when:
				def baselLineIFPPlanPreferencesPage = new File(baseLineLocation + "IFPShopping/IFPPlanPreferencesPage.png")
				if(req.IFPPlanPreferencesPage.equalsIgnoreCase("yes")){
					def IFPPlanPreferencesPageImage = "User takes screenshot"(fileDir,req.IFPPlanPreferencesPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPPlanPreferencesPageImage,baselLineIFPPlanPreferencesPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPlanPreferencesPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPlanPreferencesPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Summary tab"()

			then:
				at IFPSummaryPage

			when:
				def baselLineIFPSummaryPage = new File(baseLineLocation + "IFPShopping/IFPSummaryPage.png")
				if(req.IFPSummaryPage.equalsIgnoreCase("yes")){
					def IFPSummaryPageImage = "User takes screenshot"(fileDir,req.IFPSummaryPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(IFPSummaryPageImage,baselLineIFPSummaryPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPSummaryPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPSummaryPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Pick A Plan button"()

			then:
				at IFPPlanListPage

				def baselLineIFPPlanListPage = new File(baseLineLocation + "IFPShopping/IFPPlanListPage.png")
				if(req.IFPSummaryPage.equalsIgnoreCase("yes")){
					def IFPSummaryPageImage = "User takes screenshot"(fileDir,req.IFPSummaryPage,this.getClass().getSimpleName())
					if(!imgcomp.compare_images(IFPSummaryPageImage,baselLineIFPSummaryPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPlanListPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("IFPPlanListPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}

				email.sendEmail(req.UserEmail, "IFPShopping ScreenShots", filePath)

				break;

			case(System.getProperty("geb.build.medicare")):
				browser.setBaseUrl(req.URL)
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				Date date = new Date()
				def currentDateTime = dateFormat.format(date).toString().replaceAll(  "/","-").replaceAll(" ","_").replaceAll(":",".")
				def fileDir = location + "\\"  + currentDateTime + "_"+ req.UserEmail.toString().split("@")[0] + "\\"
				def filePath = "file:" + location + "/"  + currentDateTime  + "_" + req.UserEmail.toString().split("@")[0] + "/"
				def imgcomp= new ImageCompare()
				def email = new EmailUtility()
				def statusFileLocation = fileDir + "\\" + "StatusReport.txt"
				File statusFile = new File(statusFileLocation)
				if(!statusFile.exists()) {
					statusFile.getParentFile().mkdirs()
					statusFile.createNewFile()
				}

			when :
				to MedicareHomePage

				def baselLineImageMedicareHomePage = new File(baseLineLocation + "Medicare/MedicareHomePage.png")
				if(req.MedicareHomePage.equalsIgnoreCase("yes")){
					def MedicareHomePageImage="User takes screenshot"(fileDir,req.MedicareHomePage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicareHomePageImage,baselLineImageMedicareHomePage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareHomePage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareHomePage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on GetStarted btn"()

			then:
				at MedicareMethodPage

			when:
				def baselLineImageMedicareMethodPage = new File(baseLineLocation + "Medicare/MedicareMethodPage.png")
				if(req.MedicareMethodPage.equalsIgnoreCase("yes")){
					def MedicareMethodPageImage="User takes screenshot"(fileDir,req.MedicareMethodPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicareMethodPageImage,baselLineImageMedicareMethodPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareMethodPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareMethodPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on HelpMeChoose btn"()

			then:
				at MedicareGetStartedPage

			when:
				def baselLineImageMedicareGetStartedPage = new File(baseLineLocation + "Medicare/MedicareGetStartedPage.png")
				if(req.MedicareGetStartedPage.equalsIgnoreCase("yes")){
					def MedicareGetStartedPageImage="User takes screenshot"(fileDir,req.MedicareGetStartedPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicareGetStartedPageImage,baselLineImageMedicareGetStartedPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareGetStartedPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareGetStartedPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User enters Zip code"(zipCode)
				"User clicks on Continue btn"()

			then:
				at MedicareAddDrugsPage

			when:
				def baselLineImageMedicareAddDrugsPage = new File(baseLineLocation + "Medicare/MedicareAddDrugsPage.png")
				if(req.MedicareAddDrugsPage.equalsIgnoreCase("yes")){
					def MedicareAddDrugsPageImage="User takes screenshot"(fileDir,req.MedicareAddDrugsPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicareAddDrugsPageImage,baselLineImageMedicareAddDrugsPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareAddDrugsPage image same with baseline image\n")
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareAddDrugsPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Skip button"()

			then:
				1==1

				email.sendEmail(req.UserEmail, "Medicare ScreenShots", filePath)

				break;

			case(System.getProperty("geb.build.medicarePPC")):
				browser.setBaseUrl(req.URL)
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
				Date date = new Date()
				def currentDateTime = dateFormat.format(date).toString().replaceAll(  "/","-").replaceAll(" ","_").replaceAll(":",".")
				def fileDir = location + "\\"  + currentDateTime + "_"+ req.UserEmail.toString().split("@")[0] +"\\"
				def filePath = "file:" + location + "/"  + currentDateTime  + "_" + req.UserEmail.toString().split("@")[0] + "/"
				def imgcomp= new ImageCompare()
				def email = new EmailUtility()
				def statusFileLocation = fileDir + "\\" + "StatusReport.txt"
				File statusFile = new File(statusFileLocation)
				if(!statusFile.exists()) {
					statusFile.getParentFile().mkdirs()
					statusFile.createNewFile()
				}

			when :
				to MedicarePPCHomePage

				def baselLineImageMedicarePPCHomePage = new File(baseLineLocation + "MedicarePPC/MedicarePPCHomePage.png")
				if(req.MedicarePPCHomePage.equalsIgnoreCase("yes")){
					def MedicarePPCHomePageImage="User takes screenshot"(fileDir,req.MedicarePPCHomePage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCHomePageImage,baselLineImageMedicarePPCHomePage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCHomePage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCHomePage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User enters username and password"(loginCredentials)
				"User clicks on Login button"()

			then:
				at MedicarePPCAgentDashBoardpage

			when:
				def baselLineImageMedicarePPCAgentDashBoardpage = new File(baseLineLocation + "MedicarePPC/MedicarePPCAgentDashBoardpage.png")
				if(req.MedicarePPCAgentDashBoardpage.equalsIgnoreCase("yes")){
					def MedicarePPCAgentDashBoardpageImage="User takes screenshot"(fileDir,req.MedicarePPCAgentDashBoardpage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCAgentDashBoardpageImage,baselLineImageMedicarePPCAgentDashBoardpage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAgentDashBoardpage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAgentDashBoardpage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User selects startConsultation Tab"()

			then:
				at MedicareStartConsultationPage

			when:
				def baselLineImageMedicareStartConsultationPage = new File(baseLineLocation + "MedicarePPC/MedicareStartConsultationPage.png")
				if(req.MedicareStartConsultationPage.equalsIgnoreCase("yes")){
					def MedicareStartConsultationPageImage="User takes screenshot"(fileDir,req.MedicareStartConsultationPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicareStartConsultationPageImage,baselLineImageMedicareStartConsultationPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareStartConsultationPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicareStartConsultationPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on SOA tab"()

			then:
				at MedicarePPCSOAPage

			when:
				def baselLineImageMedicarePPCSOAPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCSOAPage.png")
				if(req.MedicarePPCSOAPage.equalsIgnoreCase("yes")){
					def MedicarePPCSOAPageImage="User takes screenshot"(fileDir,req.MedicarePPCSOAPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCSOAPageImage,baselLineImageMedicarePPCSOAPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCSOAPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCSOAPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Health tab"()

			then:
				at MedicarePPCGetStartedPage

			when:
				def baselLineImageMedicarePPCGetStartedPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCGetStartedPage.png")
				if(req.MedicarePPCGetStartedPage.equalsIgnoreCase("yes")){
					def MedicarePPCGetStartedPageImage="User takes screenshot"(fileDir,req.MedicarePPCGetStartedPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCGetStartedPageImage,baselLineImageMedicarePPCGetStartedPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCGetStartedPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCGetStartedPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Subsidy tab"()

			then:
				at MedicarePPCSubsidyPage

			when:
				def baselLineImageMedicarePPCSubsidyPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCSubsidyPage.png")
				if(req.MedicarePPCSubsidyPage.equalsIgnoreCase("yes")){
					def MedicarePPCSubsidyPageImage="User takes screenshot"(fileDir,req.MedicarePPCSubsidyPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCSubsidyPageImage,baselLineImageMedicarePPCSubsidyPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCSubsidyPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCSubsidyPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on drugs tab"()

			then:
				at MedicarePPCAddDrugsPage

			when:
				def baselLineImageMedicarePPCAddDrugsPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCAddDrugsPage.png")
				if(req.MedicarePPCAddDrugsPage.equalsIgnoreCase("yes")){
					def MedicarePPCAddDrugsPageImage="User takes screenshot"(fileDir,req.MedicarePPCAddDrugsPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCAddDrugsPageImage,baselLineImageMedicarePPCAddDrugsPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAddDrugsPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAddDrugsPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on Pharmacy tab"()

			then:
				at MedicarePPCAddPharmacyPage

			when:
				def baselLineImageMedicarePPCAddPharmacyPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCAddPharmacyPage.png")
				if(req.MedicarePPCAddPharmacyPage.equalsIgnoreCase("yes")){
					def MedicarePPCAddPharmacyPageImage="User takes screenshot"(fileDir,req.MedicarePPCAddPharmacyPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCAddPharmacyPageImage,baselLineImageMedicarePPCAddPharmacyPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAddPharmacyPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCAddPharmacyPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}
				"User clicks on ComparePlans tab"()

			then:
				at MedicarePPCComparePlansPage

				def baselLineImageMedicarePPCComparePlansPage = new File(baseLineLocation + "MedicarePPC/MedicarePPCComparePlansPage.png")
				if(req.MedicarePPCComparePlansPage.equalsIgnoreCase("yes")){
					def MedicarePPCComparePlansPageImage="User takes screenshot"(fileDir,req.MedicarePPCComparePlansPage,this.getClass().getSimpleName())
					if(imgcomp.compare_images(MedicarePPCComparePlansPageImage,baselLineImageMedicarePPCComparePlansPage)){
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCComparePlansPage image same with baseline image\n")
						out.newLine()
						out.close()
					}else{
						BufferedWriter out = new BufferedWriter(new FileWriter(statusFile , true))
						out.write("MedicarePPCComparePlansPage image different from baseline image\n")
						out.newLine()
						out.close()
					}
				}else{
				}

				email.sendEmail(req.UserEmail, "MedicarePPC ScreenShots", filePath)

				break;
		}

		where:
		req|url
		lmp[0]|lmp[0].URL
		lmp[1]|lmp[1].URL
		lmp[2]|lmp[2].URL
	}
	def username = "amohan"
	def password = "Anu@2017"
	def demographicDetails = [
		ZipCode : "90010",
		FirstName : "Ricky",
		LastName : "Pointing",
		DOB :"11/11/1957"
	]

	def loginCredentials = [
		userName:"TestUser50",
		password:"123456"
	]

	def zipCode = "90010"

	def location = "//172.16.138.24/UHG-Docs/Automation/ScreenShotUtility/ScreenShot"

	def baseLineLocation = "//172.16.138.24/UHG-Docs/Automation/ScreenShotUtility/baseLineImages/"

	@Shared def rf= new readfiles()
	@Shared def lmp = rf.readFromMultipleExcel()
}