package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PCongratsMessageAndBenefitsNavigationPage extends ConnecturePage{
	static url = "HouseHold/Dashboard"
	
	static content = {
		verifyCongratsText(wait:true){$("div[class='gradientauto']")}
		benefitsTab(wait:true){$("a[href='/PlatformIFP/HouseHold/Benefit']")[0]}
		copyRightText(wait:true){$("p[class='copyright']")}
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.DentalPageTitle
		   waitFor(){copyRightText.isDisplayed()}
	 }
	
	

	def "User verify cngrats message & navigate to Benefits tab"() {
		waitFor(){verifyCongratsText.isDisplayed()}
		benefitsTab.click()
	}
	
		
}
