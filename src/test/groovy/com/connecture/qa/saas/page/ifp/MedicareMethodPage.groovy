package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicareMethodPage extends ConnecturePage {

	static url = "PlanCompare/Consumer/Type1/2017/Compare/Method"
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h = new ScreenShotHelper()
	
	static content = {
		helpMeChoose(wait:true){$("a[class='btn-primary method-btn']")[0]}
		howDoYouWantToShopForMedicareCoverage(wait:true){$('[id="widget_EWlNrWj5UkiDb3svjqN8PQ"] > h2.h2')[0]}
	}
	
	static at = {
		helpMeChoose.isDisplayed()
		howDoYouWantToShopForMedicareCoverage.isDisplayed()
		
	}
	
	def "User clicks on HelpMeChoose btn"(){
		waitFor(){helpMeChoose.isDisplayed()}
		helpMeChoose.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes")){
			if(helpMeChoose.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(howDoYouWantToShopForMedicareCoverage.isDisplayed()) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}
	
	}else{
		
		}
}}
