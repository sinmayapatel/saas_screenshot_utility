package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class IFPHealthQuestionsPage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/CoverageSummary"
	
	static content = {
		continueToDoctorsAndFacilities(wait:true){$("a[id='autonav_Doctors_p']")[0]}
		howIsYourFamilyHealth(wait:true){$('h1')[0]}	
	}
	
	static at = {
		continueToDoctorsAndFacilities.isDisplayed()
		howIsYourFamilyHealth.isDisplayed()
	}
	
	def "User clicks on 'Continue to Doctors and Facilities button"(){
		waitFor(){continueToDoctorsAndFacilities.isDisplayed()}
		continueToDoctorsAndFacilities.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		if(pageRequired.equalsIgnoreCase("yes"))
		{
			if(continueToDoctorsAndFacilities.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					if(howIsYourFamilyHealth.text().contains("How is your family's health?")) {
						return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
					}
			}
		}else{
		
		}
	
	}
}}
