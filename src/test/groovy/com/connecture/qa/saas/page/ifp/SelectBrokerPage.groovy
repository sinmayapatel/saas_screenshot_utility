package com.connecture.qa.saas.page.ifp

import geb.module.*

import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper

class SelectBrokerPage extends ConnecturePage{
	def pageName = this.getClass().getSimpleName()
	static url = "Steps/SelectBroker"
	
	ScreenShotHelper h= new ScreenShotHelper()

	static Content = {
		selectYourBrokerDropDown(wait:true){$("div[class='drx-widget IFP_SelectYourBroker']>div>div>div>div[id='brokeSelectPanel']>select[id='brokerDropdown']").module(Select)}
		brokerSelectionText(wait:true){$('h1')}
		continueToShopping(wait:true){$("a[class='btn leftBlk ConfigurablePageBtn']")[0]}
	}

	static at = {
		selectYourBrokerDropDown.isDisplayed()
	}

	def "User Select Broker From Drop down"(){
		selectYourBrokerDropDown.selected = "Test, Broker"
	}
	
	def "User clicks on Coninue to Shopping button"(){
		waitFor(){continueToShopping.isDisplayed()}
		continueToShopping.click()
	}

	def "User takes screenshot"(specName,pageRequired,currentSpecName){
		
		if(pageRequired.equalsIgnoreCase("yes")){
			if(brokerSelectionText.isDisplayed()) {
				if(driver.currentUrl.toString().contains(url)) {
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,currentSpecName)
				}
			}
		}else{
		
		}
	
	}
}