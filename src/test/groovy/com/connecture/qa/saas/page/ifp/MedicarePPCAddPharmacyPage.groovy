package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCAddPharmacyPage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/AddPharmacy"
	
	static content = {
		addPharmacyText(wait:true){$('h1')[1]}
		comparePlansTab(wait:true){$("a[title='Step 7 of 9 is for Compare Plans']")[0]}
	}
	
	static at = {
		addPharmacyText.isDisplayed()
	}
	
	def "User clicks on ComparePlans tab"(){
		waitFor(){comparePlansTab.isDisplayed()}
		comparePlansTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}
}
		

