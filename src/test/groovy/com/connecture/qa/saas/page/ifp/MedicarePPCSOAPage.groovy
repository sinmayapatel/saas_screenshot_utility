package com.connecture.qa.saas.page.ifp

import com.connecture.qa.saas.config.ConnecturePage;
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper;

class MedicarePPCSOAPage extends ConnecturePage {
	
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	
	static url = "PlanCompare/Professional/Type1/2017/Compare/SOA"
	
	static content = {
		scopeOfAppointmentText(wait:true){$('h1')[0]}
		healthTab(wait:true){$("a[title='Step 3 of 9 is for Health']")[0]}
	}
	
	static at = {
		scopeOfAppointmentText.isDisplayed()
	}
	
	def "User clicks on Health tab"(){
		waitFor(){healthTab.isDisplayed()}
		healthTab.click()
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){

		if(pageRequired.equalsIgnoreCase("yes")){
			return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
		}else{
		}
	}
}

