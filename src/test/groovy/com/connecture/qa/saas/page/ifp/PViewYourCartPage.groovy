package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PViewYourCartPage extends ConnecturePage{
	static url = "Compare/CoverageOptions"
	
	static content = {
		viewYourCartTab(wait:true){$("a[data-tabname='View Your Cart']")}
		continueToEnrollmentBtn(wait:true){$("a[id='btnenroll']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		dentalTab(wait:true){$("ul[class='coverageGuideTab']>li:nth-child(2)>a")}
		waivedCoverageText(wait:true){$("tr[class='coverageSummary_row_waived']>td:nth-child(5)")}
		continueToviewYourCart(wait:true){$("#autonav_BenefitSummary")}
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.CartPageTitle
		   waitFor(){copyRightText.isDisplayed()}
	 }
	
	

	def "User click continue to enrollment Btn"() {
		waitFor(){viewYourCartTab.isDisplayed()}
		continueToEnrollmentBtn.click()
	}
	
	def "User validates continue to enrollment Btn is enabled or disabled"(status) { 
		def flag
		if(status == "Enabled")
		{
		flag = !continueToEnrollmentBtn.@disabled
		}
		else
		if(status == "Disabled")
		{
		flag = continueToEnrollmentBtn.@disabled
		}
		return flag
	}
	
	def "User clicks on the dental tab"(){
		waitFor(){dentalTab.isDisplayed()}
		dentalTab.click()
	}
	
	def "User wiaved coverage text is present"(){
		waitFor(){waivedCoverageText.isDisplayed()}
		waivedCoverageText.text() == "Waived Coverage"
		return true
	}
	
	def "User click continue to Cart Btn"(){
		waitFor(){continueToviewYourCart.isDisplayed()}
		continueToviewYourCart.click()
	}
		
}
