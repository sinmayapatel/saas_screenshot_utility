package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PVerifyEmpActPage extends ConnecturePage{
	static url = "Compare/Home"
	
	static content = {
		EmployeePinTextBox(wait:true){$("input[id='AuthCode_Field1']").module(TextInput)}
		lastNameTextBox(wait:true){$("input[id='AuthCode_Field2']").module(TextInput)}
		dateOfBirthTextBox(wait:true){$("input[id='AuthCode_Field3']").module(TextInput)}
		employeeIDTextBox(wait:true){$("input[id='AuthCode_Field4']").module(TextInput)}
		verifyBtn(wait:true){$("input[value='Verify']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
		
	}
	
	static at = {
		   title == clientConfig.saasClientSpecificInfo.IFP.QA.MVPHomePageTitle
		   copyRightText.isDisplayed()
	 }
	
	

	def "User enters credentials and logs in"(loginData) {
		EmployeePinTextBox << loginData.userPin
		lastNameTextBox << loginData.lastName
		dateOfBirthTextBox << loginData.dob
		employeeIDTextBox << loginData.employerId
		waitFor(){verifyBtn.isDisplayed()}
		verifyBtn.click()
	}
	
		
}
