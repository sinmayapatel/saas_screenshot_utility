package com.connecture.qa.saas.page.ifp
import java.io.File.*

import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.ifp.utils.ScreenShotHelper


class IFPPlanListPage extends ConnecturePage {
	def pageName = this.getClass().getSimpleName()
	ScreenShotHelper h= new ScreenShotHelper()
	static url = "Steps/ViewPlans"
	
	static content = {
		completingTheGuidedHelpCouldSaveMoneyAndHelpYouFindThePerfectPlan(wait:true){$('span.h2')[0]}
	}

	static at = {
		//completingTheGuidedHelpCouldSaveMoneyAndHelpYouFindThePerfectPlan.isDisplayed()
		1==1
	}
	
	def "User takes screenshot"(specName,pageRequired,downloadDir){
		
	if(pageRequired.equalsIgnoreCase("yes")){
					return h.takeScreenshotSinglePage(this.browser.driver,specName,pageName,downloadDir)
	}else{
		
		}
			}
		}
