package com.connecture.qa.saas.page.ifp
import com.connecture.qa.saas.config.ConnecturePage
import com.connecture.qa.saas.config.ClientConfiguration
import geb.module.*

/**
 * @Author: Marks <jmarks@connecture.com>
 * @Created Date: 01/18/2017
 * @version: 1.0
 */
class PQuickMemberRegistrationPage extends ConnecturePage {
	//static url = "Member/QuickRegister"
	
	static content = {
		yourProfileBtn(wait:true){$("a[title='1 Your profile Completed step 1 of 3']>span")}
		//verifyBtn(wait:true){$("input[value='Verify']")}
		copyRightText(wait:true){$("p[class='copyright']")}
		
		
	}
	
	static at = {
		  title == clientConfig.saasClientSpecificInfo.IFP.QA.QuickMemberRegistrationTitle
		  copyRightText.isDisplayed()
	 }
	
	

	def "User click Your Profile Icon"() {
		waitFor(){yourProfileBtn.isDisplayed()}
		yourProfileBtn.click()
	}
	
		
}
